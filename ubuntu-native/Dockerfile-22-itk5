FROM otb-ubuntu-base:22.04

ENV DEBIAN_FRONTEND noninteractive
ENV SHARK_VERSION 4.0.0

RUN echo "Europe/Paris" > /etc/timezone

RUN apt-get update -y \
 && apt-get upgrade -y \
 && apt-get install -y --no-install-recommends \
        libboost-filesystem-dev \
        libboost-graph-dev \
        libboost-program-options-dev \
        libboost-system-dev \
        libboost-thread-dev \
        libboost-test-dev \
        libcurl4-gnutls-dev \
        libexpat1-dev \
        libfftw3-dev \
        libgdal-dev \
        libgeotiff-dev \
        libgsl-dev \
        libinsighttoolkit5-dev \
        libkml-dev \
        libmuparser-dev \
        libmuparserx-dev \
        libopencv-core-dev \
        libopencv-ml-dev \
        libopenmpi-dev \
        libopenthreads-dev \
        libpng-dev \
        libsvm-dev \
        libtinyxml-dev \
        openmpi-bin \
        python3-gdal \
        ssh \
 && rm -rf /var/lib/apt/lists/*
 
 # Install custom Shark as it is not packaged anymore
RUN cd /tmp \
  && curl -s -S -L -o shark.tar.gz https://github.com/Shark-ML/Shark/archive/v${SHARK_VERSION}.tar.gz \
  && tar xzf shark.tar.gz \
  && cd Shark-${SHARK_VERSION} \
  && mkdir build \
  && cd build \
  && cmake  -DBUILD_EXAMPLES:BOOL=OFF \
            -DBUILD_TESTING:BOOL=OFF \
            -DENABLE_HDF5:BOOL=OFF \
            -DBUILD_SHARED_LIBS=ON \
            -DENABLE_CBLAS:BOOL=OFF \
            -DENABLE_OPENMP:BOOL=OFF \
            -DCMAKE_INSTALL_PREFIX:PATH=/usr \
            .. \
  && make -j 4 \
  && make install \
  && cd /tmp \
  && rm -rf Shark-${SHARK_VERSION} shark.tar.gz
